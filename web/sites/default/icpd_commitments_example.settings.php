<?php

$settings['commitments_google_sheets_integration']['source_documents'] = [
  'DOCUMENT ID GOES HERE' => [ // Get the document ID from the Sheets URL: https://docs.google.com/spreadsheets/d/{THIS IS THE DOCUMENT ID}/edit
    'tables' => [
      'Arab World', // Each of these should contain the EXACT name of the sheets tab from which the data will be pulled.
      'Africa',
      'Western Hemisphere',
      'South Asia',
      'East and South East Asia, Oceania',
      'Europe and Central Asia',
    ]
  ]
];

// Settings for the commitments_data_integration module
$settings['commitments_google_sheets_integration']['num_columns'] = 8;
$settings['commitments_google_sheets_integration']['column_map'] = [
  'country',
  'commitment',
  'abortion',
  'cse',
  'gender equality',
  'uhc',
  'sgbv',
  'reference_url'
];

$settings['commitments_google_sheets_integration']['themes_map'] = [
  'none' => '[NONE]',
  'abortion' => 'Abortion',
  'cse' => 'Comprehensive Sexuality Education',
  'gender equality' => 'Gender Equality',
  'uhc' => 'Universal Healthcare Coverage',
  'sgbv' => 'Sexual and Gender Based Violence'
];
