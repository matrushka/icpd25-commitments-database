(function ($, Drupal) {
  Drupal.behaviors.icpd25_select2 = {
    attach: function(context, settings) {
      if(context == document) {
        var $form = $('#views-exposed-form-commitments-page-1');

        if($form.length > 0) {
          var $selects = $form.find('.form-item--select2');

          $selects.each(function(index) {
            var $select = $(this);
            $select.select2({minimumResultsForSearch: 10, allowClear: false});
          });
        }
      }
    }
  }
})(jQuery, Drupal)
