<?php

namespace Drupal\commitments_data_integration\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal;
use Exception;
use Drupal\taxonomy\Entity\Term;
use Drupal\node\Entity\Node;
use Drupal\Core\Site\Settings;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class CommitmentsDataIntegrationCommands extends DrushCommands
{
    private $taxonomy_cache;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Import data from the Google Sheets document(s).
     *
     * @command commitments_data_integration:import
     * @aliases cimport
     */
    public function import()
    {
        $io = new SymfonyStyle($this->input, $this->output);

        $io->title('Import data from Google Spreadsheets');
        $io->text([
          'This command will replace all the Commitments data stored',
          'in this Drupal instance with the data found in the',
          'Google Sheets database file.',
        ]);

        if (!$io->confirm('Would you like to continue importing the data?')) {
            return;
        }

        $sheets_data = $this->fetchAllParsed();

        $this->wipeCommitments();
        $this->wipeCountriesAndRegions();
        $this->createThemes();

        foreach($sheets_data as $region_name=>$table) {
          // Create the Region taxonomy term, save its ID
          $region = $this->createOrGetTerm('region_country', $region_name);

          // Iterate through the rows of the table, and for each of them:
          foreach($table as $row) {
            $this->createCommitment($region_name, $row);
          }
        }
    }

    /**
     * Creates a new Commitment node and links it to the
     * corresponding taxonomy terms
     *
     * @param array $commitment_data
     * @return object Commitment node
     */
    private function createCommitment($region_name, $commitment_data)
    {
        $io = new SymfonyStyle($this->input, $this->output);
        $config = $this->getConfiguration();

        // Create the Country if it does not exist, make it a child of the parent Region
        $country = $this->createOrGetTerm('region_country', $commitment_data['country'], $region_name);

        // Create the Commitment node
        $node_data = [
          'type' => 'icpd_commitment',
          'title' => "{$commitment_data['country']} - " . mb_substr($commitment_data['commitment'], 0, 100) . '...',
          'field_commitment_text' => [['value' => $commitment_data['commitment']]],
          'field_country' => [['target_id' => $country->tid->value]]
        ];

        if(!empty($commitment_data['reference_url']) && strtolower($commitment_data['reference_url']) != 'none') {
          $node_data['field_reference'] = [['uri' => $commitment_data['reference_url']]];
        }

        // Find the themes that apply for this commitment
        foreach($config['themes_map'] as $theme_key=>$theme_name) {
          if(strtolower($commitment_data[$theme_key]) == 'yes') {
            $theme_term = $this->createOrGetTerm('themes', $theme_name);
            $theme_tid = $theme_term->tid->value;
            $node_data['field_theme'][] = ['target_id' => $theme_tid];
          }
        }

        if(empty($node_data['field_theme'])) {
          $theme_none = $this->createOrGetTerm('themes', $config['themes_map']['none']);
          $theme_none_tid = $theme_none->tid->value;
          $node_data['field_theme'] = [['target_id' => $theme_none_tid]];
        }

        Node::create($node_data)->save();
    }

    /**
      * If the term does not exist, creates a new term with the given
      * $term_name in the $vocabulary specified.
      * If the term exists, just returns the term object.
      *
      * @param string $vocabulary
      * @param string $term_name
      * @param string $term_parent
      *
      * @return Term
      */
    private function createOrGetTerm($vocabulary, $term_name, $parent_term_name=null)
    {
      $existent = taxonomy_term_load_multiple_by_name($term_name, $vocabulary);

      if($term = array_shift($existent)) {
        return $term;
      }
      else {

        $new_term = Term::create([
          'name' => $term_name,
          'vid' => $vocabulary
        ]);

        if($parent_term_name && $parent_term = array_shift(taxonomy_term_load_multiple_by_name($parent_term_name, $vocabulary))) {
          $parent_term_tid = $parent_term->tid->value;
          $new_term->parent = ['target_id' => $parent_term_tid];
        }

        $new_term->save();

        return $new_term;
      }
    }

    /**
     * Import data from the Google Sheets document(s).
     *
     * @command commitments_data_integration:test
     * @aliases ctest
     */
    public function testConnection()
    {
        $io = new SymfonyStyle($this->input, $this->output);
        $io->title('Test the connection to the Google Sheets database');
        $io->text([
          'I (whoever that is) will now fetch the data from the Sheets database',
          'and present you with a sample of a few rows from each table.',
          '',
          'This will help you confirm that the data we are getting',
          'for each of the regions is the right one.',
          '',
          'If there is any problem, the configuration will need to',
          'be adjusted. Talk to the developers.',
        ]);

        $tables = $this->fetchAllParsed();
        $io->success('Got all tables without any issues.');
    }

    /**
     * Gets the row data and parses the CSV strings into arrays
     *
     * @return array Keyed by table name, each item holds a full table array
     */
    private function fetchAllParsed()
    {
        $io = new SymfonyStyle($this->input, $this->output);
        $config = $this->getConfiguration();

        $raw = $this->fetchAll();

        $tables = [];
        $with_errors = false;
        foreach ($raw as $table_name => $table_raw_data) {
            $stream = fopen('php://memory', 'r+');
            fwrite($stream, $table_raw_data);
            rewind($stream);

            $table = [];
            while(($csv_row = fgetcsv($stream)) !==FALSE) {
              $table[] = $csv_row;
            }

            $table = $this->cleanTable($table, $table_name);

            $mapped_table = [];
            foreach($table as $row) {
              $mapped_row = [];
              foreach($config['column_map'] as $column_number => $field_name) {
                $mapped_row[$field_name] = $row[$column_number];
              }
              $mapped_table[] = $mapped_row;
            }

            // Discard the headers row
            array_shift($mapped_table);

            $tables[$table_name] = $mapped_table;
        }

        return $tables;
    }

    /**
     * Make any checks that are necessary that the contents of a
     * table are something we can work with.
     * This will discard any rows that have problems.
     *
     * @param array $table The array with all the table rows
     * @param string $table_name The name of the table. It's just used for displaying validation warnings
     * @return array $table The table with all the problematic rows removed
     */
    private function cleanTable(array $table, string $table_name)
    {
        $io = new SymfonyStyle($this->input, $this->output);
        $config = $this->getConfiguration();

        // Ensure that no row is missing data or shorter for other reasons (no newline parsing problems)
        $problematic_rows = [];
        $filtered_table = array_filter($table, function ($row, $i) use ($config, &$problematic_rows) {
            $has_enough_columns = count($row) >= $config['num_columns'];

            $commitment_column_number = array_search('commitment', $config['column_map']);
            $commitment_is_not_empty = !empty($row[$commitment_column_number]);

            $country_column_number = array_search('country', $config['column_map']);
            $country_is_not_empty = !empty($row[$country_column_number]);

            $passes = $has_enough_columns && $commitment_is_not_empty && $country_is_not_empty;

            if(!$passes) {
              $problematic_rows[] = $i+1;
            }

            return $passes;

        }, ARRAY_FILTER_USE_BOTH);

        $rows_removed = count($table) - count($filtered_table);
        if($rows_removed > 0) {
          $io->warning("$rows_removed rows were removed from the table $table_name.");
          $io->text("The problem rows are: " . implode(', ', $problematic_rows));
        }

        return $filtered_table;
    }

    /**
     * Gets all the data from Sheets according to configuration,
     * returns an array with all of it.
     *
     * @return array
     */
    private function fetchAll()
    {
        $io = new SymfonyStyle($this->input, $this->output);
        $client = Drupal::httpClient();

        $config = $this->getConfiguration();

        $tables = [];
        foreach ($config['source_documents'] as $source_document => $source_document_config) {
            $table_names = $source_document_config['tables'];
            foreach ($table_names as $table_name) {
                $csv_url = "https://docs.google.com/spreadsheets/d/{$source_document}/gviz/tq";
                $response = $client->request('GET', $csv_url, ['query' => [
                  'sheet' => $table_name,
                  'tqx' => 'out:csv'
                ]]);

                if ($response->getStatusCode() == 200) {
                    $tables[$table_name] = (string) $response->getBody();
                    $io->success("Got table $table_name");
                } else {
                    $io->error("Getting the table $table_name failed with code {$response->getStatusCode()}");
                }
            }
        }

        return $tables;
    }

    /**
      * Destroys all the Commitment nodes and wipes the
      * Region-Country taxonomy vocabulary.
      *
      * Note that the Themes vocabulary will not be cleaned
      * as that data is not expected to change, at all,
      * and the source of the Theme names is in the configuration.
      */
    private function wipeCommitments()
    {
        // Destroy all the nodes
        $commitments_query = Drupal::entityQuery('node');
        $commitments_query->condition('type', 'icpd_commitment');
        $commitment_nids = $commitments_query->execute();

        $storage_handler = Drupal::entityTypeManager()->getStorage('node');
        $entities = $storage_handler->loadMultiple($commitment_nids);
        $storage_handler->delete($entities);
    }

    private function wipeCountriesAndRegions()
    {
        $region_query = Drupal::entityQuery('taxonomy_term');
        $region_query->condition('vid', 'region_country');
        $region_tids = $region_query->execute();


        $storage_handler = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
        $entities = $storage_handler->loadMultiple($region_tids);
        $storage_handler->delete($entities);
    }

    /**
     * Creates the Themes taxonomy terms according to configuration
     */
    private function createThemes()
    {
      $config = $this->getConfiguration();
      foreach($config['themes_map'] as $key=>$theme_name) {
        $this->createOrGetTerm('themes', $theme_name);
      }
    }

    private function getConfiguration()
    {
        $config = Settings::get('commitments_google_sheets_integration');
        return $config;
    }

    /**
    EXAMPLES -- DISCARD
    */


  // /**
  //  * An example of the table output format.
  //  *
  //  * @param array $options An associative array of options whose values come from cli, aliases, config, etc.
  //  *
  //  * @field-labels
  //  *   group: Group
  //  *   token: Token
  //  *   name: Name
  //  * @default-fields group,token,name
  //  *
  //  * @command commitments_data_integration:token
  //  * @aliases token
  //  *
  //  * @filter-default-field name
  //  * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
  //  */
  // public function token($options = ['format' => 'table']) {
  //   $all = \Drupal::token()->getInfo();
  //   foreach ($all['tokens'] as $group => $tokens) {
  //     foreach ($tokens as $key => $token) {
  //       $rows[] = [
  //         'group' => $group,
  //         'token' => $key,
  //         'name' => $token['name'],
  //       ];
  //     }
  //   }
  //   return new RowsOfFields($rows);
  // }
}
