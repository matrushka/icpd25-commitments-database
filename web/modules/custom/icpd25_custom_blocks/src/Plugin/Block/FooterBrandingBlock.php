<?php

namespace Drupal\icpd25_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Footer branding' Block.
 *
 * @Block(
 *   id = "footer_branding_block",
 *   admin_label = @Translation("Footer branding"),
 *   category = @Translation("icpd25 blocks"),
 * )
 */
class FooterBrandingBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $code = 'footerbranding';
    return array(
      '#theme' => 'freeform',
      '#code' => $code
    );
  }

}
